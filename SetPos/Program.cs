﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices; // Dllimport, LayoutKind
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetPos
{
	class Program
	{
		///////////////////////////////
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr FindWindow(String sClassName, String sWindowText);
		///////////////////////////////
		[StructLayout(LayoutKind.Sequential, Pack = 4)]
		public struct RECT32 { public int left; public int top; public int right; public int bottom; }
		///////////////////////////////
		[DllImport("user32.dll")]
		private static extern bool GetWindowRect(IntPtr hWnd, out RECT32 lpRect);
		///////////////////////////////
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int uFlags);
		private const int HWND_BOTTOM = 1;
		private const int HWND_TOP = 0;
		private const int HWND_TOPMOST = -1;
		private const int HWND_NOTOPMOST = -2;
		private const int SWP_NOSIZE = 0x0001;
		private const int SWP_NOMOVE = 0x0002;
		///////////////////////////////
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern int ShowWindow(IntPtr hWnd, int nCmdShow);
		private const int SW_MAXIMIZE = 3;
		private const int SW_MINIMIZE = 6;
		private const int SW_RESTORE  = 9;
		///////////////////////////////
		static void Main(string[] args)
		{
			RECT32 rect;
			foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("chrome"))
			{
				IntPtr hWnd = p.MainWindowHandle;
				if (hWnd != IntPtr.Zero)
				{
					GetWindowRect(hWnd, out rect);
					System.Console.WriteLine("{0},{1}", rect.left, rect.top);
					if (rect.top < 0)
					{
						ShowWindow(hWnd, SW_RESTORE);
						System.Threading.Thread.Sleep(200);
						SetWindowPos(hWnd, HWND_TOP, 32, 0, 0, 0, SWP_NOSIZE);
					}
					else
					{
						SetWindowPos(hWnd, HWND_TOP, -8, -1088, 0, 0, SWP_NOSIZE);
						ShowWindow(hWnd, SW_MAXIMIZE);
					}
				}
			}
			//SetWindowPos(hWnd, HWND_TOP, 1920, -1080, 0, 0, SWP_NOSIZE);
		}
	}
}
