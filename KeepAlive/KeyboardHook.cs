using System;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace HongliangSoft.Utilities.Gui
{
	///<summary>キーボードが操作されたときに実行されるメソッドを表すイベントハンドラ。</summary>
	public delegate void KeyboardHookedEventHandler(object sender, KeyboardHookedEventArgs e);
	///<summary>KeyboardHookedイベントのデータを提供する。</summary>
	public class KeyboardHookedEventArgs : CancelEventArgs {
		///<summary>新しいインスタンスを作成する。</summary>
		internal KeyboardHookedEventArgs(KeyboardMessage message, ref KeyboardState state) {
			this.message = message;
			this.state = state;
		}
		private KeyboardMessage message;
		private KeyboardState state;
		///<summary>キーボードが押されたか放されたかを表す値を取得する。</summary>
		public KeyboardUpDown UpDown {
			get {
				return (message == KeyboardMessage.KeyDown || message == KeyboardMessage.SysKeyDown) ?
					KeyboardUpDown.Down : KeyboardUpDown.Up;
			}
		}
		///<summary>操作されたキーの仮想キーコードを表す値を取得する。</summary>
		public Keys KeyCode {get {return state.KeyCode;}}
		///<summary>操作されたキーのスキャンコードを表す値を取得する。</summary>
		public int ScanCode {get {return state.ScanCode;}}
		///<summary>操作されたキーがテンキーなどの拡張キーかどうかを表す値を取得する。</summary>
		public bool IsExtendedKey {get {return state.Flag.IsExtended;}}
		///<summary>ALTキーが押されているかどうかを表す値を取得する。</summary>
		public bool AltDown {get {return state.Flag.AltDown;}}
	}
	///<summary>キーボードが押されているか放されているかを表す。</summary>
	public enum KeyboardUpDown {
		///<summary>キーは押されている。</summary>
		Down,
		///<summary>キーは放されている。</summary>
		Up,
	}

	///<summary>メッセージコードを表す。</summary>
	internal enum KeyboardMessage {
		///<summary>キーが押された。</summary>
		KeyDown    = 0x100,
		///<summary>キーが放された。</summary>
		KeyUp      = 0x101,
		///<summary>システムキーが押された。</summary>
		SysKeyDown = 0x104,
		///<summary>システムキーが放された。</summary>
		SysKeyUp   = 0x105,
	}
	///<summary>キーボードの状態を表す。</summary>
	internal struct KeyboardState {
		///<summary>仮想キーコード。</summary>
		public Keys KeyCode;
		///<summary>スキャンコード。</summary>
		public int ScanCode;
		///<summary>各種特殊フラグ。</summary>
		public KeyboardStateFlag Flag;
		///<summary>このメッセージが送られたときの時間。</summary>
		public int Time;
		///<summary>メッセージに関連づけられた拡張情報。</summary>
		public IntPtr ExtraInfo;
	}
	///<summary>キーボードの状態を補足する。</summary>
	internal struct KeyboardStateFlag {
		private int flag;
		private bool IsFlagging(int value) {
			return (flag & value) != 0;
		}
		private void Flag(bool value, int digit) {
			flag = value ? (flag | digit) : (flag & ~digit);
		}
		///<summary>キーがテンキー上のキーのような拡張キーかどうかを表す。</summary>
		public bool IsExtended {get {return IsFlagging(0x01);} set {Flag(value, 0x01);}}
		///<summary>イベントがインジェクトされたかどうかを表す。</summary>
		public bool IsInjected {get {return IsFlagging(0x10);} set {Flag(value, 0x10);}}
		///<summary>ALTキーが押されているかどうかを表す。</summary>
		public bool AltDown {get {return IsFlagging(0x20);} set {Flag(value, 0x20);}}
		///<summary>キーが放されたどうかを表す。</summary>
		public bool IsUp {get {return IsFlagging(0x80);} set {Flag(value, 0x80);}}
	}
	///<summary>キーボードの操作をフックし、任意のメソッドを挿入する。</summary>
	[DefaultEvent("KeyboardHooked")]
	public class KeyboardHook : Component {
		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr SetWindowsHookEx(int hookType, KeyboardHookDelegate hookDelegate, IntPtr hInstance, uint threadId);
		[DllImport("user32.dll", SetLastError=true)]
		private static extern int CallNextHookEx(IntPtr hook, int code, KeyboardMessage message, ref KeyboardState state);
		[DllImport("user32.dll", SetLastError=true)]
		private static extern bool UnhookWindowsHookEx(IntPtr hook);

		private delegate int KeyboardHookDelegate(int code, KeyboardMessage message, ref KeyboardState state);
		private const int KeyboardHookType = 13;
		private GCHandle hookDelegate;
		private IntPtr hook;
		private static readonly object EventKeyboardHooked = new object();
		///<summary>キーボードが操作されたときに発生する。</summary>
		public event KeyboardHookedEventHandler KeyboardHooked {
			add {base.Events.AddHandler(EventKeyboardHooked, value);}
			remove {base.Events.RemoveHandler(EventKeyboardHooked, value);}
		}
		///<summary>
		///KeyboardHookedイベントを発生させる。
		///</summary>
		///<param name="e">イベントのデータ。</param>
		protected virtual void OnKeyboardHooked(KeyboardHookedEventArgs e) {
			KeyboardHookedEventHandler handler = base.Events[EventKeyboardHooked] as KeyboardHookedEventHandler;
			if (handler != null)
				handler(this, e);
		}
		///<summary>
		///新しいインスタンスを作成する。
		///</summary>
		public KeyboardHook() {
			if (Environment.OSVersion.Platform != PlatformID.Win32NT)
				throw new PlatformNotSupportedException("Windows 98/Meではサポートされていません。");
			KeyboardHookDelegate callback = new KeyboardHookDelegate(CallNextHook);
			this.hookDelegate = GCHandle.Alloc(callback);
			IntPtr module = Marshal.GetHINSTANCE(typeof(KeyboardHook).Assembly.GetModules()[0]);
			this.hook = SetWindowsHookEx(KeyboardHookType, callback, module, 0);
		}
		///<summary>
		///キーボードが操作されたときに実行するデリゲートを指定してインスタンスを作成する。
		///</summary>
		///<param name="handler">キーボードが操作されたときに実行するメソッドを表すイベントハンドラ。</param>
		public KeyboardHook(KeyboardHookedEventHandler handler) : this() {
			this.KeyboardHooked += handler;
		}
		private int CallNextHook(int code, KeyboardMessage message, ref KeyboardState state) {
			if (code >= 0) {
				KeyboardHookedEventArgs e = new KeyboardHookedEventArgs(message, ref state);
				OnKeyboardHooked(e);
				if (e.Cancel) {
					return -1;
				}
			}
			return CallNextHookEx(IntPtr.Zero, code, message, ref state);
		}
		///<summary>
		///使用されているアンマネージリソースを解放し、オプションでマネージリソースも解放する。
		///</summary>
		///<param name="disposing">マネージリソースも解放する場合はtrue。</param>
		protected override void Dispose(bool disposing) {
			if (this.hookDelegate.IsAllocated) {
				UnhookWindowsHookEx(hook);
				this.hook = IntPtr.Zero;
				this.hookDelegate.Free();
			}
			base.Dispose(disposing);
		}
	}
	///<summary>マウスが入力されたときに実行されるメソッドを表すイベントハンドラ。</summary>
	public delegate void MouseHookedEventHandler(object sender, MouseHookedEventArgs e);
	///<summary>MouseHookedイベントのデータを提供する。</summary>
	public class MouseHookedEventArgs : EventArgs {
		///<summary>
		///新しいインスタンスを作成する。
		///</summary>
		///<param name="message">マウス操作の種類を表すMouseMessage値の一つ。</param>
		///<param name="state">マウスの状態を表すMouseState構造体。</param>
		internal MouseHookedEventArgs(MouseMessage message, ref MouseState state) {
			this.message = message;
			this.state = state;
		}
		private MouseMessage message;
		private MouseState state;
		///<summary>マウス操作の種類を表すMouseMessage値。</summary>
		public MouseMessage Message {get {return message;}}
		///<summary>スクリーン座標における現在のマウスカーソルの位置。</summary>
		public Point Point {get {return state.Point;}}
		///<summary>ホイールの情報を表すWheelData構造体。</summary>
		public WheelData WheelData {get {return state.WheelData;}}
		///<summary>XButtonの情報を表すXButtonData構造体。</summary>
		public XButtonData XButtonData {get {return state.XButtonData;}}
	}
	///<summary>マウス操作の種類を表す。</summary>
	public enum MouseMessage {
		///<summary>マウスカーソルが移動した。</summary>
		Move         = 0x200,
		///<summary>左ボタンが押された。</summary>
		LDown        = 0x201,
		///<summary>左ボタンが解放された。</summary>
		LUp          = 0x202,
		///<summary>右ボタンが押された。</summary>
		RDown        = 0x204,
		///<summary>左ボタンが解放された。</summary>
		RUp          = 0x205,
		///<summary>中ボタンが押された。</summary>
		MDown        = 0x207,
		///<summary>中ボタンが解放された。</summary>
		MUp          = 0x208,
		///<summary>ホイールが回転した。</summary>
		Wheel        = 0x20A,
		///<summary>Xボタンが押された。</summary>
		XDown        = 0x20B,
		///<summary>Xボタンが解放された。</summary>
		XUp          = 0x20C,
	}
	///<summary>マウスの状態を表す。</summary>
	[StructLayout(LayoutKind.Explicit)] internal struct MouseState {
		///<summary>スクリーン座標によるマウスカーソルの現在位置。</summary>
		[FieldOffset(0)]
		public Point Point;
		///<summary>messageがMouseMessage.Wheelの時にその詳細データを持つ。</summary>
		[FieldOffset(8)]
		public WheelData WheelData;
		///<summary>messageがMouseMessage.XDown/MouseMessage.XUpの時にその詳細データを持つ。</summary>
		[FieldOffset(8)]
		public XButtonData XButtonData;
		///<summary>マウスのイベントインジェクト。</summary>
		[FieldOffset(12)]
		public MouseStateFlag Flag;
		///<summary>メッセージが送られたときの時間</summary>
		[FieldOffset(16)]
		public int Time;
		///<summary>メッセージに関連づけられた拡張情報</summary>
		[FieldOffset(20)]
		public IntPtr ExtraInfo;
	}
	///<summary>マウスホイールの状態の詳細を表す。</summary>
	public struct WheelData {
		///<summary>ビットデータ。</summary>
		public int State;
		///<summary>ホイールの回転一刻みを表す。</summary>
		public static readonly int OneWheel = 120;
		///<summary>ホイールの回転量を表す。クリックされたときは-1。</summary>
		public int WheelDelta {
			get {
				int delta = State >> 16;
				return (delta < 0) ? -delta : delta;
			}
		}
		///<summary>ホイールが一刻み分動かされたかどうかを表す。</summary>
		public bool IsOneWheel {get {return (State >> 16) == OneWheel;}}
		///<summary>ホイールの回転方向を表す。</summary>
		public WheelDirection Direction {
			get {
				int delta = State >> 16;
				if (delta == 0) return WheelDirection.None;
				return (delta < 0) ? WheelDirection.Backward : WheelDirection.Forward;
			}
		}
	}
	///<summary>ホイールの回転方向を表す。</summary>
	public enum WheelDirection {
		///<summary>回転していない。</summary>
		None     =  0,
		///<summary>ユーザから離れる方向へ回転した。</summary>
		Forward  =  1,
		///<summary>ユーザに近づく方向へ回転した。</summary>
		Backward = -1,
	}
	///<summary>Xボタンの状態の詳細を表す。</summary>
	public struct XButtonData {
		///<summary>ビットデータ。</summary>
		public int State;
		///<summary>操作されたボタンを示す。</summary>
		public int ControlledButton {get {return State >> 16;}}
		///<summary>Xボタン1が押されたかどうかを示す。</summary>
		public bool IsXButton1 {get {return (State >> 16) == 1;}}
		///<summary>Xボタン2が押されたかどうかを示す。</summary>
		public bool IsXButton2 {get {return (State >> 16) == 2;}}
	}
	///<summary>マウスの状態を補足する。</summary>
	internal struct MouseStateFlag {
		///<summary>ビットデータ。</summary>
		public int Flag;
		///<summary>イベントがインジェクトされたかどうかを表す。</summary>
		public bool IsInjected {
			get {return (Flag & 1) != 0;}
			set {Flag = value ? (Flag | 1) : (Flag & ~1);}
		}
	}
	///<summary>マウスをフックし、任意のメソッドを実行する。</summary>
	public class MouseHook : System.ComponentModel.Component {
		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr SetWindowsHookEx(int hookType, MouseHookDelegate hookDelegate, IntPtr hInstance, uint threadId);
		[DllImport("user32.dll", SetLastError=true)]
		private static extern int CallNextHookEx(IntPtr hook, int code, MouseMessage message, ref MouseState state);
		[DllImport("user32.dll", SetLastError=true)]
		private static extern bool UnhookWindowsHookEx(IntPtr hook);
		private const int MouseLowLevelHook = 14;
		private delegate int MouseHookDelegate(int code, MouseMessage message, ref MouseState state);
		private MouseHookDelegate hookDelegate;
		private IntPtr hook;
		private static readonly object EventMouseHooked = new object();
		///<summary>マウスが入力されたときに発生する。</summary>
		public event MouseHookedEventHandler MouseHooked  {
			add {base.Events.AddHandler(EventMouseHooked, value);}
			remove {base.Events.RemoveHandler(EventMouseHooked, value);}
		}
		///<summary>
		///インスタンスを作成する。
		///</summary>
		public MouseHook() {
			if (Environment.OSVersion.Platform != PlatformID.Win32NT)
				throw new PlatformNotSupportedException("Windows 98/Meではサポートされていません。");
			this.hookDelegate = new MouseHookDelegate(CallNextHook);
			IntPtr module = Marshal.GetHINSTANCE(typeof(MouseHook).Assembly.GetModules()[0]);
			hook = SetWindowsHookEx(MouseLowLevelHook, hookDelegate, module, 0);
		}
		///<summary>
		///マウスが入力されたときに実行するデリゲートを指定してインスタンスを作成する。
		///</summary>
		///<param name="handler">マウスが入力されたときに実行するメソッドを表すデリゲート。</param>
		public MouseHook(MouseHookedEventHandler handler) : this() {
			this.MouseHooked += handler;
		}
		///<summary>
		///MouseHookedイベントを発生させる。
		///</summary>
		///<param name="e">イベントのデータ。</param>
		protected virtual void OnMouseHooked(MouseHookedEventArgs e) {
			MouseHookedEventHandler invoked = Events[EventMouseHooked] as MouseHookedEventHandler;
			if (invoked != null)
				invoked(this, e);
		}
		private int CallNextHook(int code, MouseMessage message, ref MouseState state) {
			if (code >= 0) {
				OnMouseHooked(new MouseHookedEventArgs(message, ref state));
			}
			return CallNextHookEx(hook, code, message, ref state);
		}
		///<summary>
		///使用されているアンマネージリソースを解放し、オプションでマネージリソースも解放する。
		///</summary>
		///<param name="disposing">マネージリソースも解放する場合はtrue。</param>
		protected override void Dispose(bool disposing) {
			if (!disposed) {
				disposed = true;
				UnhookWindowsHookEx(hook);
				hook = IntPtr.Zero;
				base.Dispose(disposing);
			}
		}
		private bool disposed = false;
	}
}
