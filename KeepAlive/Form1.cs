﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices; // Dllimport
using HongliangSoft.Utilities.Gui; // KeyboardHooked
using Windows.UI.Notifications;
using Windows.UI.Notifications.Management;
using static InputSimulator;

namespace KeepAlive
{
	public partial class Form1 : System.Windows.Forms.Form
	{
		private static Form1 sForm1;
		private static string CurrentDir = System.AppDomain.CurrentDomain.BaseDirectory;
		private static System.Diagnostics.DefaultTraceListener sDtl;
		private static System.Windows.Forms.Timer timer1 = null;
		//private static System.Windows.Forms.Timer timer2 = null;
		private static bool IsKeepAlived = false;
		private static bool IsScreenSaverDisable = false;
		private static DateTime sKeepSt;
		private static DateTime sKeepEt;
		private static KeyboardHook sKeyHook;
		private static System.Threading.SemaphoreSlim sSemaphore = new System.Threading.SemaphoreSlim(1);
		private static System.Threading.Tasks.Task sFormNotificationListener;

		public Form1()
		{
			sForm1 = this;
			// コンソールログをファイルに出力
			string logfile = Environment.GetEnvironmentVariable("TEMP") + @"\FFXIVLOG_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".txt";
			System.IO.File.Delete(logfile);
			sDtl = (System.Diagnostics.DefaultTraceListener)System.Diagnostics.Trace.Listeners["Default"];
			sDtl.LogFileName = logfile;
			System.Diagnostics.Debug.WriteLine(logfile);
			InitializeComponent();
		}

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern bool SystemParametersInfo(int action, int uiParam, ref int lpvParam, int flags); 
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern bool SystemParametersInfo(int action, int uiParam, IntPtr pvParam, int fWinIni);
		private const int SPI_GETSCREENSAVEACTIVE = 16;
		private const int SPI_SETSCREENSAVEACTIVE = 17;
		private const int SPI_GETSCREENSAVETIMEOUT = 14;
		private const int SPI_SETSCREENSAVETIMEOUT = 15;
		private const int SPI_GETSCREENSAVERRUNNING = 114;
		private const int SPIF_SENDWININICHANGE = 2;
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern int GetClassName(IntPtr hWnd, StringBuilder s, int nMaxCount);

		//////////////////////////////////////////////////////////////////////
		//
		//
		public static void print(object s)
		{
			print("{0}", s);
		}
		public static void print(string p, params object[] args)
		{
			var str = String.Format(p, args);
			System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str);
			sForm1.textBox1.AppendText(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str +"\r\n");
		}
		public static void dprint(string p, params object[] args)
		{
			if (sForm1.checkBox4.Checked == false) return;
			print(p,args);
		}
		public static void dprint(object s)
		{
			if (sForm1.checkBox4.Checked == false) return;
			print("{0}", s);
		}

		private bool IsSSHLaunched()
		{
			//return true;
			//System.Diagnostics.Process[] ps = System.Diagnostics.Process.GetProcessesByName("ssh");
			//if (ps.Length>0) {dprint("SSH running detected."); return true;}
			var now = DateTime.Now;
			if (now.DayOfWeek == DayOfWeek.Saturday) return false;
			if (now.DayOfWeek == DayOfWeek.Sunday) return false;
			#if false
			var t = now.Hour*100 + now.Minute;
			if (this.checkBox4.Checked) dprint("{0}", t);
			if (t >= 1200 && t  < 1300) return false;
			if (t >= 1030 && t < 2130) return true;
			#else
			var pick1 = this.dateTimePicker1.Value;
			var pick2 = this.dateTimePicker2.Value;
			if (pick1 != sKeepSt || pick2 != sKeepEt)
			{
				print($"Suck {pick1} != {sKeepSt}");
				print($"Suck {pick2} != {sKeepEt}");
				this.dateTimePicker1.Value = sKeepSt;
				this.dateTimePicker2.Value = sKeepEt;
			}
			var tsnow = now.TimeOfDay;
			var tsstart = this.dateTimePicker1.Value.TimeOfDay;
			var tsend = this.dateTimePicker2.Value.TimeOfDay;
			if (now.Hour==12) return false;
			if (tsnow > tsstart && tsnow < tsend) {dprint("TRUE");return true;}
			#endif
			return false;
		}

		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr FindWindow(String sClassName, String sWindowText);
		[DllImport("user32.dll", EntryPoint = "FindWindowEx")]
		public static extern IntPtr FindWindowEx(IntPtr hWnd, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr GetForegroundWindow();
		[DllImport("user32.dll", SetLastError = true)]
		public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);
		//---- SendMessage
		public const int WM_PASTE = 0x0302;
		[DllImport("user32.dll", CharSet=CharSet.Auto)]
		public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, IntPtr lParam);

		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int uFlags);
		private const int HWND_BOTTOM = 1;
		private const int HWND_TOP = 0;
		private const int HWND_TOPMOST = -1;
		private const int HWND_NOTOPMOST = -2;
		private const int SWP_NOSIZE = 0x0001;
		private const int SWP_NOMOVE = 0x0002;
		private const int SWP_SHOWWINDOW = 0x0040;
		private const int SWP_NOACTIVATE = 0x0010;
		[DllImport("user32.dll")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		private void SetActiveWindow(IntPtr hWnd)
		{
			int tpid = 0;
			int tthid = 0;
			int retry = 0;
			tthid = GetWindowThreadProcessId(hWnd, out tpid);
			// スレッドのインプット状態を結び付ける
			//if (fthid !=0 && fthid != tthid) if (AttachThreadInput(tthid, fthid, true)==false) print("AttachThreadInput - NG");
			while(GetForegroundWindow()!=hWnd)
			{
				SetForegroundWindow(hWnd);
				retry++;
			}
			{
				SetWindowPos(hWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
				SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOMOVE|SWP_NOSIZE);
			}
		}

		private bool KeepAlive()
		{
			{
				string[] names = {"firefox"};
				var sb = new StringBuilder(256);
				foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
				{
					if (p.MainWindowHandle == IntPtr.Zero) dprint("-[{0}] {1}/{2}", p.MainWindowHandle, p.ProcessName, p.MainWindowTitle);
					if (p.MainWindowHandle != IntPtr.Zero)
					{
						dprint("+[{0}] {1}/{2}", p.MainWindowHandle, p.ProcessName, p.MainWindowTitle);
						if (GetClassName(p.MainWindowHandle, sb, sb.Capacity)==0) continue;
						if (names.Contains(sb.ToString()) == false && names.Contains(p.ProcessName) == false) continue;
						print("[{0}] {1}/{2}", p.MainWindowHandle, p.ProcessName, p.MainWindowTitle);
						IntPtr hWnd = p.MainWindowHandle;
						IntPtr rWnd = hWnd;
						while(rWnd != IntPtr.Zero)
						{
							print("[{0}] SendKey VK_HOME", rWnd);
							InputSimulator.SendKey(rWnd, VK_HOME);
							if (hWnd==rWnd) rWnd = IntPtr.Zero;
							rWnd = FindWindowEx(hWnd, rWnd, null, null);
						}
					}
				}
			}
			return false;
		}

		#if true
		private void PostUserForm(string pwtext)
		{
			print(pwtext);
			if (this.checkBox5.Checked)
				System.Windows.Forms.SendKeys.Send("0300800012{TAB}");
			System.Windows.Forms.SendKeys.Send(pwtext);
			//var previous = System.Windows.Forms.Clipboard.GetText();
			//System.Windows.Forms.Clipboard.SetText(pwtext);
			//TaskDelay(previous);
		}
		// 非同期処理本体
		private async void TaskDelay(string previous)
		{
			await System.Threading.Tasks.Task.Delay(200);
			System.Windows.Forms.SendKeys.Send("^v");
			System.Windows.Forms.SendKeys.Send("0300800012{TAB}2023R05-0901");
			await System.Threading.Tasks.Task.Delay(1000);
			if (previous != string.Empty)
				System.Windows.Forms.Clipboard.SetText(previous);
			else
				System.Windows.Forms.Clipboard.Clear();
			print("Clipboard.Clear");
		}
		#endif
		#if false
		private void PostUserForm(string pwtext)
		{
			print(pwtext);
			var previous = System.Windows.Forms.Clipboard.GetText();
			System.Windows.Forms.Clipboard.SetText(pwtext);
			if (this.checkBox3.Checked)
			{
				var sb = new StringBuilder(256);
				IntPtr hWnd = GetForegroundWindow();
				IntPtr rWnd = hWnd;
				while(true)
				{
					if (rWnd == IntPtr.Zero) break;
					if (GetClassName(rWnd, sb, sb.Capacity)!=0)
					{
						print("WM_PASTE [{0}] {1}", rWnd, sb.ToString());
						SendMessage(rWnd, WM_PASTE, 0, IntPtr.Zero);
					}
					if (hWnd==rWnd) rWnd = IntPtr.Zero;
					rWnd = FindWindowEx(hWnd, rWnd, null, null);
				}
				TaskDelay(previous);
			}
			//var player = new System.Media.SoundPlayer(@"C:\Windows\Media\Windows Logon.wav");
			//player.Play();
		}
		// 非同期処理本体
		private async void TaskDelay(string previous)
		{
			await System.Threading.Tasks.Task.Delay(3000);
			if (previous != string.Empty)
				System.Windows.Forms.Clipboard.SetText(previous);
			else
				System.Windows.Forms.Clipboard.Clear();
			print("Clipboard.Clear");
		}
		#endif

		private static DateTime sDtOnPress = DateTime.Now;
		private static System.Collections.Generic.List<System.Windows.Forms.Keys> modifierState = new System.Collections.Generic.List<System.Windows.Forms.Keys>();
		//private static System.Collections.Generic.List<Keys> previousState = new System.Collections.Generic.List<Keys>();
		private static System.Windows.Forms.Keys[] modifiers = {
			System.Windows.Forms.Keys.ControlKey, System.Windows.Forms.Keys.Menu, System.Windows.Forms.Keys.ShiftKey,
			System.Windows.Forms.Keys.LControlKey, System.Windows.Forms.Keys.LMenu, System.Windows.Forms.Keys.LShiftKey,
			System.Windows.Forms.Keys.RControlKey, System.Windows.Forms.Keys.RMenu, System.Windows.Forms.Keys.RShiftKey,
		};
		private static Keys previousKeyCode = 0;
		private static void keyHookProcWrapper(object sender, KeyboardHookedEventArgs e)
		{
			IntPtr hWnd = IntPtr.Zero;
			if (sForm1.checkBox4.Checked) print("[{0}] {1}", e.KeyCode, e.UpDown);
			if (e.KeyCode == Keys.MediaPlayPause) e.Cancel = true;
			if (modifiers.Contains(e.KeyCode)) {
				if (e.UpDown == KeyboardUpDown.Up && modifierState.Contains(e.KeyCode)==true) modifierState.Remove(e.KeyCode);
				if (e.UpDown == KeyboardUpDown.Down && modifierState.Contains(e.KeyCode)==false)
				{
					modifierState.Add(e.KeyCode);
					//previousState = new System.Collections.Generic.List<Keys>(modifierState); // For using CTRL + ALT + D1/D2
				}
				return;
			}
			if (sSemaphore.CurrentCount == 0) {
				//e.Cancel = true;
				if (sForm1.checkBox4.Checked) print("Reject");
				return;
			}
			sSemaphore.Wait();
			//if (e.KeyCode == Keys.P && modifierState.Contains(Keys.LControlKey)==true && modifierState.Contains(Keys.LMenu)==true)
			if (e.KeyCode == Keys.Pause)
			{
				if (sForm1.checkBox3.Checked)
				{
					e.Cancel = true;
					if (e.UpDown == KeyboardUpDown.Down)
					{
						sDtOnPress = DateTime.Now;
					}
					if (e.UpDown == KeyboardUpDown.Up)
					{
						print("PAUSE");
						//if ((DateTime.Now - sDtOnPress).TotalSeconds < 2)
						{
							DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
							var pwtext = String.Format("{0:D4}R{1:D2}-{2:D2}{3:D2}ex", now.Year, now.Year-2018, (now.Month-1)/4*4+1, 1);
							sForm1.PostUserForm(pwtext);
						}
//						else
//						{
//							DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
//							now = now.AddDays(-56);
//							var pwtext = String.Format("{0:D4}R{1:D2}-{2:D2}{3:D2}", now.Year, now.Year-2018, (now.Month-1)/2*2+1, 1);
//							sForm1.PostUserForm(pwtext);
//						}
					}
				}
			}
			else if (e.KeyCode == Keys.PrintScreen) // Fn + I
			{
				if (sForm1.checkBox3.Checked)
				{
					e.Cancel = true;
					if (e.UpDown == KeyboardUpDown.Up)
					{
						var sb = new StringBuilder(256);
						GetClassName(GetForegroundWindow(), sb, sb.Capacity);
						print(sb.ToString());
						sForm1.PostUserForm("0300800012");
					}
				}
			}
			else if (e.KeyCode == Keys.MediaPlayPause)
			{
				print("MediaPlayPause {0}", e.UpDown);
				e.Cancel = true;
				if (e.UpDown == KeyboardUpDown.Down)
				{
					sForm1.HeadSetMuteToggle();
				}
			}
			//////////////////////////////////////////////////////////////////////
			// Send IR Signal with MouseWithoutBorders
			#if false
			if ((e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2) && modifierState.Contains(Keys.LMenu)==true && modifierState.Contains(Keys.LControlKey)==true)
			{
				if (previousKeyCode != e.KeyCode && sForm1.checkBox6.Checked)
				{
					print("CTRL+ALT+{0}", e.KeyCode);
					sForm1.TaskDelay_HDMI(e.KeyCode == Keys.D1 ? 1:2);
				}
			}
			#endif
			//previousState = new System.Collections.Generic.List<Keys>(modifierState); // For using CTRL + ALT + D1/D2
			previousKeyCode = (e.UpDown == KeyboardUpDown.Down) ? e.KeyCode:0;
			sSemaphore.Release();
		}

		#if false
		private void ScreenSaverDisable(bool is_disable)
		{
			if (is_disable==true && IsScreenSaverDisable==false)
			{
				IntPtr nullVar = IntPtr.Zero;
				Int32 value = 0;
				SystemParametersInfo(SPI_GETSCREENSAVERTIMEOUT, 0, ref value, 0); 
				print("ScreenSaver Current Time {0}", value);
				SystemParametersInfo( SPI_SETSCREENSAVERTIMEOUT, 120*60 /* 120min */, IntPtr.Zero, SPIF_SENDWININICHANGE);
				SystemParametersInfo( SPI_SETSCREENSAVERACTIVE, 0, IntPtr.Zero, SPIF_SENDWININICHANGE);
				print("ScreenSaver Disable");
				IsScreenSaverDisable = true;
			}
			if (is_disable==false && IsScreenSaverDisable==true)
			{
				SystemParametersInfo( SPI_SETSCREENSAVERTIMEOUT, 15*60 /* 15min */, IntPtr.Zero, SPIF_SENDWININICHANGE);
				SystemParametersInfo( SPI_SETSCREENSAVERACTIVE, 1, IntPtr.Zero, SPIF_SENDWININICHANGE);
				print("ScreenSaver Active");
				IsScreenSaverDisable = false;
			}
		}
		#else
		private void ScreenSaverDisable(bool is_disable)
		{
			if (is_disable==true && IsScreenSaverDisable==false)
			{
				IntPtr nullVar = IntPtr.Zero;
				Int32 value = 0;
				SystemParametersInfo( SPI_SETSCREENSAVEACTIVE, 0, IntPtr.Zero, SPIF_SENDWININICHANGE);
				print("ScreenSaver Disable");
				SystemParametersInfo(SPI_GETSCREENSAVETIMEOUT, 0, ref value, 0); 
				print("ScreenSaver Current Time: {0}", value);
				if (sForm1.ProcessExecute("powercfg.exe", "/X monitor-timeout-ac 240"))
				{
					print("TurnOff the Disbplay Time(AC): {0}", 240);
					IsScreenSaverDisable = true;
				}
			}
			if (is_disable==false && IsScreenSaverDisable==true)
			{
				IntPtr nullVar = IntPtr.Zero;
				Int32 value = 0;
				SystemParametersInfo(SPI_SETSCREENSAVETIMEOUT, 900 /* 15min */, IntPtr.Zero, SPIF_SENDWININICHANGE);
				SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, 1, IntPtr.Zero, SPIF_SENDWININICHANGE);
				print("ScreenSaver Enable");
				SystemParametersInfo(SPI_GETSCREENSAVETIMEOUT, 0, ref value, 0); 
				print("ScreenSaver Current Time: {0}", value);
				IsScreenSaverDisable = false;
			}
		}
		private static bool IsHeadSetMuted = false;
		private void HeadSetMuteToggle()
		{
			TaskDelay_HeadSetMuteToggle();
		}
		private async void TaskDelay_HeadSetMuteToggle()
		{
			await System.Threading.Tasks.Task.Delay(200);
			if (IsHeadSetMuted)
			{
				print("HeadSet Unmute");
				sForm1.ProcessExecute(CurrentDir + @"Resources\SoundVolumeView.exe", "/Unmute \"Sound Blaster JAM V2 Hands-Free AG Audio\\Device\\HeadSet\\Capture\"");
				IsHeadSetMuted = false;
				var player = new System.Media.SoundPlayer(CurrentDir + @"Resources\Windows Hardware Insert.wav");
				player.Play();
			}
			else
			{
				print("HeadSet Mute");
				sForm1.ProcessExecute(CurrentDir + @"Resources\SoundVolumeView.exe", "/Mute \"Sound Blaster JAM V2 Hands-Free AG Audio\\Device\\HeadSet\\Capture\"");
				IsHeadSetMuted = true;
				var player = new System.Media.SoundPlayer(CurrentDir + @"Resources\Windows Hardware Remove.wav");
				player.Play();
			}
		}
		//////////////////////////////////////////////////////////////////////
		// Nature-remo にHDMI切り替え信号を送る
		//
		private async void TaskDelay_HDMI(int channel, bool isLocal = false)
		{
			await System.Threading.Tasks.Task.Delay(20);
			if (channel==1)
			{
				if (isLocal)
				{
					string data = "{\"format\":\"us\",\"freq\":40,\"data\""
					+ ":[7985,4053,440,569,414,593,438,1574,438,567,440,567,445,1567,439,1573,443,1571,445,4074,413,595,440,1573,438,"
					+ "1579,438,1578,436,576,438,1574,441,568,438,569,436,19093,7983,4055,439,570,412,593,445,1567,436,576,439,566,"
					+ "414,1600,414,1601,442,1576,435,4087,414,593,437,1572,437,1577,438,1576,414,593,414,1604,440,571,414,593,433]}";
					data = data.Replace("\"", "\\\"");
					sForm1.ProcessExecute(@"C:\Windows\System32\curl.exe", $"-X POST \"http://remo-mini-b/messages\" -H \"X-Requested-With: local\" -H \"accept: application/json\" -H \"Content-Type: application/json\" -d \"{data}\"");
				}
				else
				{
					string data  = @"-X POST https://api.nature.global/1/signals/b38e9df7-8fce-4667-adb4-01246bba7711/send";
					data += " -H \"accept: application/json\" -H \"authorization: Bearer 35i5MzIp9huX2tfCjqVYj3Pz7_CXu8vzCun0W4qyvXU.oOGxDzmC0X7IJP828y5R9WLCeANHWsE5yXmDOwU9FIQ\"";
					sForm1.ProcessExecute(@"C:\Windows\System32\curl.exe", data);
				}

			}
			else if (channel==2)
			{
				if (isLocal)
				{
					string data = "{\"format\":\"us\",\"freq\":40,\"data\""
					+ ":[8019,4038,470,538,471,538,472,1529,486,541,471,538,471,1530,486,1528,488,1547,472,4037,486,523,486,523,486,"
					+ "1528,488,539,470,1531,486,1532,486,528,486,523,486,20053,8017,4024,486,538,471,538,471,1528,488,541,471,538,"
					+ "471,1528,488,1531,486,1545,472,4050,471,525,486,523,486,1546,471,544,473,1526,486,1546,471,538,471,538,470]}";
					data = data.Replace("\"", "\\\"");
					sForm1.ProcessExecute(@"C:\Windows\System32\curl.exe", $"-X POST \"http://remo-mini-b/messages\" -H \"X-Requested-With: local\" -H \"accept: application/json\" -H \"Content-Type: application/json\" -d \"{data}\"");
				}
				else
				{
					string data  = "-X POST \"https://api.nature.global/1/signals/5455c2c7-1147-4b7f-b49c-fb352ebc9990/send\"";
					data += " -H \"accept: application/json\" -H \"authorization: Bearer 35i5MzIp9huX2tfCjqVYj3Pz7_CXu8vzCun0W4qyvXU.oOGxDzmC0X7IJP828y5R9WLCeANHWsE5yXmDOwU9FIQ\"";
					sForm1.ProcessExecute(@"C:\Windows\System32\curl.exe", data);
				}
			}
		}
		private bool ProcessExecute(string exe, string param)
		{
			dprint("{0} {1}", exe, param);
			var p = new System.Diagnostics.Process();
			p.StartInfo.FileName = exe;
			p.StartInfo.Arguments = param;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.Start();
			p.WaitForExit();
			var result = p.ExitCode;
			p.Dispose();
			return result == 0 ? true:false;
		}
		#endif

		private struct LASTINPUTINFO {public uint cbSize; public uint dwTime;}
		[DllImport("User32.dll")]
		private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
		public static int GetLastInputTimeSec()
		{
			LASTINPUTINFO lastInPut = new LASTINPUTINFO();
	        lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
			if (!GetLastInputInfo(ref lastInPut))
			{
				return 0;
			}
			return (System.Environment.TickCount - (int)lastInPut.dwTime) / 1000;
		}

		//////////////////////////////////////////////////////////////////////
		//
		private static async System.Threading.Tasks.Task TaskDelay_NotificationListener()
		{
			await System.Threading.Tasks.Task.Delay(200);
			if (!Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.Notifications.Management.UserNotificationListener"))
			{
				print("[UWP] IsTypePresent: NG");
				return;
			}
			UserNotificationListener listener = UserNotificationListener.Current;
			print("[UWP] listener: {0}", listener);
			UserNotificationListenerAccessStatus accessStatus = await listener.RequestAccessAsync();
			print("[UWP] accessStatus: {0}", accessStatus);
			if (accessStatus != UserNotificationListenerAccessStatus.Allowed)
			{
				print("[UWP] Access denied.");
				return;
			}
			print("[UWP] Starting Notification Listener.");
			DateTime rtime = DateTime.Now;
			while(true)
			{
				await System.Threading.Tasks.Task.Delay(1000);
				//if (sFormCTS.Token.IsCancellationRequested) break;
				if (sForm1.checkBox6.Checked == false) continue;
				System.Collections.Generic.IReadOnlyList<UserNotification> notifs = await listener.GetNotificationsAsync(NotificationKinds.Toast);
				foreach (var n in notifs)
				{
					NotificationBinding toastBinding = n.Notification.Visual.GetBinding(KnownNotificationBindings.ToastGeneric);
					if (toastBinding != null)
					{
						if (n.CreationTime.DateTime <= rtime) continue;
						System.Collections.Generic.IReadOnlyList<AdaptiveNotificationText> textElements = toastBinding.GetTextElements();
						var titleText = textElements.FirstOrDefault()?.Text;
						var bodyText = string.Join("\n", textElements.Skip(1).Select(t => t.Text));
						if (titleText=="Mouse without Borders")
						{
							int hdmi = 0;
							if (bodyText.Contains("Ctrl+Alt+1")) hdmi = 1;
							if (bodyText.Contains("Ctrl+Alt+2")) hdmi = 2;
							print("{0}", n.CreationTime.DateTime);
							print("Title: {0}", titleText);
							print("Body: {0}", bodyText);
							if (hdmi>0)
							{
								sForm1.TaskDelay_HDMI(hdmi);
							}
						}
						rtime = n.CreationTime.DateTime;
					}
				}
			}
			return;
		}
		//////////////////////////////////////////////////////////////////////
		//
		// Form API
		//
		private void Form1_Load(object sender, EventArgs e)
		{
			Properties.Settings.Default.Reload();
			{
				this.checkBox1.Checked = Properties.Settings.Default.Checkbox1;
				this.checkBox2.Checked = Properties.Settings.Default.Checkbox2;
				this.checkBox3.Checked = Properties.Settings.Default.Checkbox3;
				this.checkBox4.Checked = Properties.Settings.Default.Checkbox4;
				this.checkBox5.Checked = Properties.Settings.Default.Checkbox5;
				this.checkBox6.Checked = Properties.Settings.Default.Checkbox6;
				this.dateTimePicker1.Value = Properties.Settings.Default.Time1;
				this.dateTimePicker2.Value = Properties.Settings.Default.Time2;
				this.dateTimePicker1.Enabled = this.checkBox1.Checked;
				this.dateTimePicker2.Enabled = this.checkBox1.Checked;
			}
			var now = DateTime.Now;
			var dp1 = this.dateTimePicker1.Value;
			var dp2 = this.dateTimePicker2.Value;
			this.dateTimePicker1.Value = new DateTime(now.Year, now.Month, now.Day, dp1.Hour, dp1.Minute, dp1.Second);
			this.dateTimePicker2.Value = new DateTime(now.Year, now.Month, now.Day, dp2.Hour, dp2.Minute, dp2.Second);
			sKeepSt = this.dateTimePicker1.Value;
			sKeepEt = this.dateTimePicker2.Value;
			ScreenSaverDisable(this.checkBox2.Checked);
			print("Version 1.00");

//"C:\Users\0300800012\Home\INSTALL\soundvolumeview-x64\SoundVolumeView.exe" /Switch "{0.0.1.00000000}.{99c6e7fd-3c3f-4ca3-b287-08790dd109f6}"
///C:\Users\0300800012\Home\INSTALL\soundvolumeview-x64\SoundVolumeView.exe /Mute "Sound Blaster JAM V2 Hands-Free AG Audio\Device\ヘッドセット\Capture"

			sKeyHook = new KeyboardHook();
			sKeyHook.KeyboardHooked += new KeyboardHookedEventHandler(keyHookProcWrapper);
			var sb = new StringBuilder(256);
			foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
			{
				if (GetClassName(p.MainWindowHandle, sb, sb.Capacity)==0) continue;
				print("[{0}] ({1}/{2})", p.MainWindowHandle.ToString("x6"), sb.ToString(), p.ProcessName);
			}
			IsKeepAlived = false;
			this.WindowState = FormWindowState.Minimized;
			timer1 = new System.Windows.Forms.Timer();
			timer1.Enabled = true;
			timer1.Interval = 1000*60; // 1 minitue
			if (this.checkBox4.Checked) timer1.Interval = 1000 * 15; // 15 sec for Debug mode.
			timer1.Tick += delegate
			{
				if (this.checkBox1.Checked)
				{
					if (IsSSHLaunched())
					{
						if (KeepAlive())
						{
							print("GetLastInputTime {0}", GetLastInputTimeSec());
							IsKeepAlived = true;
							print("Keep Alive");
						}
						timer1.Interval = 1000 * 600; // 10minitues
						if (this.checkBox4.Checked) timer1.Interval = 1000 * 15; // 15 sec for Debug mode.
					}
					else if (IsKeepAlived)
					{
						IsKeepAlived = false;
						print("Keep Alive Disable");
					}
				}
				if (this.checkBox2.Checked)
				{
					Int32 value = 0;
					SystemParametersInfo(SPI_GETSCREENSAVEACTIVE, 0, ref value, 0);
					if (value != 0)
					{
						print("ScreenSaver Active {0}", value);
						IsScreenSaverDisable = false;
						ScreenSaverDisable(this.checkBox2.Checked);
					}
					SystemParametersInfo(SPI_GETSCREENSAVERRUNNING, 0, ref value, 0);
					if (value != 0)
					{
						print("ScreenSaver Running {0}", value);
						print("GetLastInputTime {0}", GetLastInputTimeSec());
					}
				}
			};
			#if false
			timer2 = new System.Windows.Forms.Timer();
			timer2.Enabled = true;
			timer2.Interval = 1000*60; // 1 minitue
			timer2.Tick += delegate
			{
				if (this.checkBox2.Checked)
				{
					ScreenSaverDisable(IsSSHLaunched());
				}
			};
			#endif
			sFormNotificationListener = TaskDelay_NotificationListener();
		}

		private void Form1_Shown(object sender, EventArgs e)
		{
			this.Hide();
		}
		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			//ScreenSaverDisable(false);
			Properties.Settings.Default.Checkbox1 = this.checkBox1.Checked;
			Properties.Settings.Default.Checkbox2 = this.checkBox2.Checked;
			Properties.Settings.Default.Checkbox3 = this.checkBox3.Checked;
			Properties.Settings.Default.Checkbox4 = this.checkBox4.Checked;
			Properties.Settings.Default.Checkbox5 = this.checkBox5.Checked;
			Properties.Settings.Default.Checkbox6 = this.checkBox6.Checked;
			Properties.Settings.Default.Time1 = this.dateTimePicker1.Value;
			Properties.Settings.Default.Time2 = this.dateTimePicker2.Value;
			Properties.Settings.Default.Save();
		}
		private void Form1_ClientSizeChanged(object sender, EventArgs e)
		{
			if (this.WindowState == System.Windows.Forms.FormWindowState.Minimized)
			{
				//print("ClientSizeChanged (Minimized)");
				// フォームが最小化の状態であればフォームを非表示にする
				this.Hide();
			}
		}
		private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
		{
			//print("notifyIcon1_MouseClick");			// フォームを表示する
			this.Visible = true;
			// 現在の状態が最小化の状態であれば通常の状態に戻す
			if (this.WindowState == FormWindowState.Minimized)
			{
				this.WindowState = FormWindowState.Normal;
			}
			this.Activate(); // フォームをアクティブにする
			this.textBox1.ScrollToCaret(); // フォーカスを行末に移動
		}
		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			var checkbox = (System.Windows.Forms.CheckBox)sender;
			this.dateTimePicker1.Enabled = checkbox.Checked;
			this.dateTimePicker2.Enabled = checkbox.Checked;
		}
		private void checkBox2_CheckedChanged(object sender, EventArgs e)
		{
			var checkbox = (System.Windows.Forms.CheckBox)sender;
			ScreenSaverDisable(checkbox.Checked);
		}

		private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
		{
			sKeepSt = this.dateTimePicker1.Value;
			print($"dateTimePicker1_ValueChanged {sKeepSt}");
		}

		private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
		{
			sKeepEt = this.dateTimePicker2.Value;
			print($"dateTimePicker2_ValueChanged {sKeepEt}");
		}
	}
}
