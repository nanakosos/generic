﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSpyViewer
{
	public partial class Form1 : Form
	{
		private static System.Threading.SemaphoreSlim sSemaAsync;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			sSemaAsync = new System.Threading.SemaphoreSlim(1);
			var dtst = this.dateTimePicker1.Value;
			dateTimePicker1.Value = new DateTime(dtst.Year, dtst.Month, dtst.Day);
			this.comboBox1.SelectedIndex = 0;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			TaskDelay_VLC();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			const string explorer = @"C:\Windows\explorer.exe";
			const string videoDirPath = @"\\MITTA-FORMULA\iSpy\video";
			ProcessExecuteAsync(explorer, videoDirPath);
		}

		private async void TaskDelay_VLC()
		{
			if (sSemaAsync.CurrentCount == 0) {print("Reject"); return;}
			sSemaAsync.Wait();
			await System.Threading.Tasks.Task.Delay(100);
			const string vlcplayer=@"D:\INSTALL\Drivers\IODATA_NS110W\vlc-3.0.17.4\vlc.exe";
			string camera = "*.mp4";
			float zoom = 0.867f;
			string videoDirPath = @"\\MITTA-FORMULA\iSpy\video";
			if (this.comboBox1.SelectedIndex >= 1)
				videoDirPath += "\\" + this.comboBox1.SelectedItem;
			//if (this.comboBox1.SelectedIndex==1) {camera = "1_*.mp4"; zoom = 0.867f;}
			//if (this.comboBox1.SelectedIndex==2) {camera = "3_*.mp4"; zoom = 0.867f;}
			print("{0}: {1} {2}", this.comboBox1.SelectedIndex, this.comboBox1.SelectedItem, videoDirPath);
			var dtst = this.dateTimePicker1.Value;
			var dten = this.dateTimePicker2.Value;
			var files = System.IO.Directory.GetFiles(videoDirPath, camera, System.IO.SearchOption.AllDirectories);
			var sorted = files.OrderBy(filePath => System.IO.File.GetCreationTime(filePath).Date).ThenBy(filePath => System.IO.File.GetCreationTime(filePath).TimeOfDay).ToList();
			string tmp = System.IO.Path.Combine(Environment.GetEnvironmentVariable("TEMP"), "VLC_"+DateTime.Now.ToString("yyyyMMdd-HHmmssff") + ".m3u");
 			System.IO.StreamWriter sw = new System.IO.StreamWriter(tmp, false, Encoding.UTF8);
			sw.WriteLine("#EXTM3U");
 			foreach(var file in sorted)
			{
				System.IO.FileInfo finfo = new System.IO.FileInfo(file);
				if (finfo.CreationTime >= dtst && finfo.CreationTime < dten)
				{
					//print(file);
					sw.WriteLine("#EXTINF:0,{0}", System.IO.Path.GetFileName(file));
					sw.WriteLine(String.Format("file:{0}",file).Replace("\\", "/"));
				}
			}
			sw.Close();
			ProcessExecuteAsync(vlcplayer, String.Format("{0} --rate=4 --zoom={1}", tmp, zoom));
			sSemaAsync.Release();
		}

		public void print(object s)
		{
			print("{0}", s);
		}

		public void print(string p, params object[] args)
		{
			var str = String.Format(p, args);
			System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str);
			this.textBox1.AppendText(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str +"\r\n");
		}

		private bool ProcessExecuteAsync(string exe, string param)
		{
			print("{0} {1}", exe, param);
			var p = new System.Diagnostics.Process();
			p.StartInfo.FileName = exe;
			p.StartInfo.Arguments = param;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.Start();
			p.Dispose();
			return true;
		}
	}
}
