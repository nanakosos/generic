﻿using System;
using System.Runtime.InteropServices; // LayoutKind
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static AccessTracker.Win32;
//using System.Collections.Generic; // List

namespace AccessTracker
{
	class Win32
	{
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern int GetClassName(IntPtr hWnd, StringBuilder s, int nMaxCount);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern bool IsWindow(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr GetForegroundWindow();
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr FindWindow(String sClassName, String sWindowText);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr FindWindowEx(IntPtr hWnd, IntPtr hwndChildAfter, String lpszClass, String lpszWindow);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern bool SetForegroundWindow(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern bool PostMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern Int32 SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);
		//[return: MarshalAs(UnmanagedType.Bool)]
		//private static extern bool SetForegroundWindow(IntPtr hWnd);
		[DllImport("User32.dll")]
		private static extern bool SetCursorPos(int X, int Y);
		[StructLayout(LayoutKind.Sequential, Pack = 4)]
		struct POINT32
		{
			public int X { get; set; }
			public int Y { get; set; }
			public static implicit operator System.Drawing.Point(POINT32 point) { return new System.Drawing.Point(point.X, point.Y); }
		}
		[DllImport("User32.dll")]
		private static extern bool GetCursorPos(out POINT32 lppoint);
		[DllImport("user32.dll")]
		public static extern bool IsIconic(IntPtr hWnd);
		[StructLayout(LayoutKind.Sequential, Pack = 4)]
		public struct RECT32 { public int left; public int top; public int right; public int bottom; }
		[System.Runtime.InteropServices.DllImport("User32.dll")]
		public extern static bool PrintWindow(IntPtr hwnd, IntPtr hDC, uint nFlags);
		[System.Runtime.InteropServices.DllImport("User32.dll")]
		public static extern bool GetClientRect(IntPtr hWnd, out RECT32 lpRect);
		[DllImport("user32.dll", SetLastError = true)]
		public static extern int GetWindowThreadProcessId(IntPtr hWnd,out int lpdwProcessId);
	}

	public partial class Form1 : Form
	{
		private static string sInFile = @"D:\INSTALL\LiveStream\OBS_Streaming_nginx_v1\logs\access.log";
		private static System.IO.FileStream fs = null;
		private static System.IO.TextReader sr = null;
		private static Form1 sForm1;
		//private static List<string> sViewers = new List<string>();
		private static System.Windows.Forms.Timer sTimer1 = null;
		public Form1()
		{
			sForm1 = this;
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			ScreenShot(FindWindow("chrome", null));

			this.textBox1.ForeColor = Color.White;
			this.textBox1.BackColor = Color.Black;
			string[] args = System.Environment.GetCommandLineArgs();
			if (args.Length>1) sInFile = args[0];
			if (System.IO.File.Exists(sInFile))
			{
				dprint(sInFile);
				var sViewers = new Dictionary<string, int>();
				fs = new System.IO.FileStream( sInFile, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
				sr = new System.IO.StreamReader(fs);
				sr.ReadToEnd();
				sViewers.Clear();
				sTimer1 = new System.Windows.Forms.Timer();
				sTimer1.Enabled = true;
				sTimer1.Interval = 1000; // 1 sec when no controller
				sTimer1.Tick += delegate
				{
					sTimer1.Interval = 1000; // 1 sec when no controller
					string line = sr.ReadLine();
					if (!System.String.IsNullOrEmpty(line))
					{
						if(line.Contains(" - - ") && line.Contains("GET "))
						{
							var visitor = line.Substring(0, line.IndexOf(" - - "));
							if (sViewers.ContainsKey(visitor)==false)
							{
								try {
									System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(visitor);
									dprint("{0} [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, host.HostName);
								}
								catch {
									dprint("{0} [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, "---");
								}
								System.Media.SystemSounds.Question.Play();
								//System.Media.SystemSounds.Hand.Play();
								//System.Media.SystemSounds.Asterisk.Play();
								//System.Media.SystemSounds.Exclamation.Play();
								//System.Media.SystemSounds.Question.Play();
							}
                            sViewers[visitor] = 10;
						}
					}
					if (sViewers.Count>0)
					{
						List<string> keysList = new List<string>(sViewers.Keys);
						foreach (var visitor in keysList)
						{
							if (sViewers[visitor]==0)
							{
								try {
									System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(visitor);
									dprint("{0} Exit [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, host.HostName);
								}
								catch { // System.Net.Sockets.SocketException
									dprint("{0} Exit [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, "---");
								}
								System.Media.SystemSounds.Hand.Play();
								sViewers.Remove(visitor);
							}
							else sViewers[visitor] = sViewers[visitor] - 1;
						}
					}
				};
			}
			else
			{
				dprint("{0} does not exist.", sInFile);
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			sForm1.ProcessExecute(@"D:\INSTALL\LiveStream\OBS_Streaming_nginx_v1\nginx.exe", "-s stop");
		}

		public static void dprint(string p, params object[] args)
		{
			var str = String.Format(p, args);
			sForm1.textBox1.AppendText(str +"\r\n");
		}

		private bool ProcessExecute(string exe, string param)
		{
			dprint(exe + " " + param);
			var p = new System.Diagnostics.Process();
			p.StartInfo.FileName = exe;
			p.StartInfo.Arguments = param;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.Start();
			p.WaitForExit();
			var result = p.ExitCode;
			p.Dispose();
			return result == 0 ? true:false;
		}
		private static void ScreenShot( IntPtr hWnd, string path = "" )
		{
			if (hWnd == null) return;
			var dtstr = DateTime.Now.ToString("yyyyMMddTHHmmss");
			Bitmap img = ScreenCapture(hWnd);
			if (img == null) return;
			if (path.Length > 0)
				img.Save( path );
			else
				img.Save($@".\Resources\" + dtstr +".PNG");
			img.Dispose();
			dprint("Saved " + $@".\Resources\" + dtstr +".PNG");
		}

		private static System.Drawing.Bitmap ScreenCapture(IntPtr hWnd)
		{
			RECT32 rout;
			if (IsIconic(hWnd) == true) return null;
			if (GetClientRect(hWnd, out rout) == false) return null;
			Bitmap bmp = new Bitmap(rout.right - rout.left + 3, rout.bottom - rout.top + SystemInformation.CaptionHeight + 3);
			Graphics memg = Graphics.FromImage(bmp);
			IntPtr dc = memg.GetHdc();
			PrintWindow(hWnd, dc, 0);
			memg.ReleaseHdc(dc);
			memg.Dispose();
			System.Drawing.Rectangle rect = new System.Drawing.Rectangle(rout.left + 3, rout.top+SystemInformation.CaptionHeight + 3, rout.right - rout.left, rout.bottom - rout.top);
			var img = bmp.Clone(rect, bmp.PixelFormat);
			bmp.Dispose();
			return img;
		}

	}
}
