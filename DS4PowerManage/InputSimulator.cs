using System;
using System.Runtime.InteropServices;

public static class InputSimulator
{
	[DllImport("user32.dll")]
	public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
	[DllImport("user32.dll")]
	public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
	[DllImport("user32.dll")]
	public static extern IntPtr PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

	public enum VirtualKeyCode
	{
		LBUTTON = 1,
		RBUTTON = 2,
		CANCEL = 3,
		MBUTTON = 4,
		XBUTTON1 = 5,
		XBUTTON2 = 6,
		BACK = 8,
		TAB = 9,
		CLEAR = 12,
		RETURN = 13,
		SHIFT = 16,
		CONTROL = 17,
		MENU = 18,
		PAUSE = 19,
		CAPITAL = 20,
		KANA = 21,
		HANGEUL = 21,
		HANGUL = 21,
		JUNJA = 23,
		FINAL = 24,
		HANJA = 25,
		KANJI = 25,
		ESCAPE = 27,
		CONVERT = 28,
		NONCONVERT = 29,
		ACCEPT = 30,
		MODECHANGE = 31,
		SPACE = 32,
		PRIOR = 33,
		NEXT = 34,
		END = 35,
		HOME = 36,
		LEFT = 37,
		UP = 38,
		RIGHT = 39,
		DOWN = 40,
		SELECT = 41,
		PRINT = 42,
		EXECUTE = 43,
		SNAPSHOT = 44,
		INSERT = 45,
		DELETE = 46,
		HELP = 47,
		VK_0 = 48,
		VK_1 = 49,
		VK_2 = 50,
		VK_3 = 51,
		VK_4 = 52,
		VK_5 = 53,
		VK_6 = 54,
		VK_7 = 55,
		VK_8 = 56,
		VK_9 = 57,
		VK_A = 65,
		VK_B = 66,
		VK_C = 67,
		VK_D = 68,
		VK_E = 69,
		VK_F = 70,
		VK_G = 71,
		VK_H = 72,
		VK_I = 73,
		VK_J = 74,
		VK_K = 75,
		VK_L = 76,
		VK_M = 77,
		VK_N = 78,
		VK_O = 79,
		VK_P = 80,
		VK_Q = 81,
		VK_R = 82,
		VK_S = 83,
		VK_T = 84,
		VK_U = 85,
		VK_V = 86,
		VK_W = 87,
		VK_X = 88,
		VK_Y = 89,
		VK_Z = 90,
		LWIN = 91,
		RWIN = 92,
		APPS = 93,
		SLEEP = 95,
		NUMPAD0 = 96,
		NUMPAD1 = 97,
		NUMPAD2 = 98,
		NUMPAD3 = 99,
		NUMPAD4 = 100,
		NUMPAD5 = 101,
		NUMPAD6 = 102,
		NUMPAD7 = 103,
		NUMPAD8 = 104,
		NUMPAD9 = 105,
		MULTIPLY = 106,
		ADD = 107,
		SEPARATOR = 108,
		SUBTRACT = 109,
		DECIMAL = 110,
		DIVIDE = 111,
		F1 = 112,
		F2 = 113,
		F3 = 114,
		F4 = 115,
		F5 = 116,
		F6 = 117,
		F7 = 118,
		F8 = 119,
		F9 = 120,
		F10 = 121,
		F11 = 122,
		F12 = 123,
		F13 = 124,
		F14 = 125,
		F15 = 126,
		F16 = 127,
		F17 = 128,
		F18 = 129,
		F19 = 130,
		F20 = 131,
		F21 = 132,
		F22 = 133,
		F23 = 134,
		F24 = 135,
		NUMLOCK = 144,
		SCROLL = 145,
		LSHIFT = 160,
		RSHIFT = 161,
		LCONTROL = 162,
		RCONTROL = 163,
		LMENU = 164,
		RMENU = 165,
		BROWSER_BACK = 166,
		BROWSER_FORWARD = 167,
		BROWSER_REFRESH = 168,
		BROWSER_STOP = 169,
		BROWSER_SEARCH = 170,
		BROWSER_FAVORITES = 171,
		BROWSER_HOME = 172,
		VOLUME_MUTE = 173,
		VOLUME_DOWN = 174,
		VOLUME_UP = 175,
		MEDIA_NEXT_TRACK = 176,
		MEDIA_PREV_TRACK = 177,
		MEDIA_STOP = 178,
		MEDIA_PLAY_PAUSE = 179,
		LAUNCH_MAIL = 180,
		LAUNCH_MEDIA_SELECT = 181,
		LAUNCH_APP1 = 182,
		LAUNCH_APP2 = 183,
		OEM_1 = 186,
		OEM_PLUS = 187,
		OEM_COMMA = 188,
		OEM_MINUS = 189,
		OEM_PERIOD = 190,
		OEM_2 = 191,
		OEM_3 = 192,
		OEM_4 = 219,
		OEM_5 = 220,
		OEM_6 = 221,
		OEM_7 = 222,
		OEM_8 = 223,
		OEM_102 = 226,
		PROCESSKEY = 229,
		PACKET = 231,
		ATTN = 246,
		CRSEL = 247,
		EXSEL = 248,
		EREOF = 249,
		PLAY = 250,
		ZOOM = 251,
		NONAME = 252,
		PA1 = 253,
		OEM_CLEAR = 254
	}

	public const uint VK_NONE = 0;
	// Modifier
	public const uint VK_CTRL = (uint)VirtualKeyCode.CONTROL;
	public const uint VK_SHIFT = (uint)VirtualKeyCode.SHIFT;
	public const uint VK_ALT  = (uint)VirtualKeyCode.MENU;
	// Special
	public const uint VK_ESC   = (uint)VirtualKeyCode.ESCAPE;
	public const uint VK_ENTER = (uint)VirtualKeyCode.RETURN;
	public const uint VK_BS = (uint)VirtualKeyCode.BACK;
	public const uint VK_BACK  = (uint)VirtualKeyCode.BACK;
	public const uint VK_HOME  = (uint)VirtualKeyCode.HOME;
	public const uint VK_UP    = (uint)VirtualKeyCode.UP;
	public const uint VK_DOWN  = (uint)VirtualKeyCode.DOWN;
	public const uint VK_LEFT  = (uint)VirtualKeyCode.LEFT;
	public const uint VK_RIGHT = (uint)VirtualKeyCode.RIGHT;
	// Functions
	public const uint VK_F1 = (uint)VirtualKeyCode.F1;
	public const uint VK_F2 = (uint)VirtualKeyCode.F2;
	public const uint VK_F3 = (uint)VirtualKeyCode.F3;
	public const uint VK_F4 = (uint)VirtualKeyCode.F4;
	public const uint VK_F5 = (uint)VirtualKeyCode.F5;
	public const uint VK_F6 = (uint)VirtualKeyCode.F6;
	public const uint VK_F7 = (uint)VirtualKeyCode.F7;
	public const uint VK_F8 = (uint)VirtualKeyCode.F8;
	public const uint VK_F9 = (uint)VirtualKeyCode.F9;
	public const uint VK_F10 = (uint)VirtualKeyCode.F10;
	public const uint VK_F11 = (uint)VirtualKeyCode.F11;
	public const uint VK_F12 = (uint)VirtualKeyCode.F12;
	// Alpha & Numeric
	public const uint VK_DECIMAL = (uint)VirtualKeyCode.DECIMAL;
	public const uint VK_INSERT = (uint)VirtualKeyCode.INSERT;
	public const uint VK_1 = (uint)VirtualKeyCode.VK_1;
	public const uint VK_A = (uint)VirtualKeyCode.VK_A;
	public const uint VK_B = (uint)VirtualKeyCode.VK_B;
	public const uint VK_T = (uint)VirtualKeyCode.VK_T;
	public const uint VK_F = (uint)VirtualKeyCode.VK_F;
	public const uint VK_G = (uint)VirtualKeyCode.VK_G;
	public const uint VK_V = (uint)VirtualKeyCode.VK_V;

	const uint WM_KEYDOWN = 0x100;
	const uint WM_KEYUP = 0x0101;
	private class ModifierKey : IDisposable
	{
		private IntPtr _gameWindow;
		private VirtualKeyCode? _modifierKey;

		public ModifierKey(IntPtr gameWindow, VirtualKeyCode? modifierKey)
		{
			this._gameWindow = gameWindow;
			this._modifierKey = modifierKey;

			if (this._modifierKey.HasValue)
			{
				KeyDown(this._gameWindow, this._modifierKey.Value);
			}
		}

		public void Dispose()
		{
			if (this._modifierKey.HasValue)
			{
				KeyUp(this._gameWindow, this._modifierKey.Value);
			}
		}
	}

	private static void KeyUp(IntPtr gameWindow, VirtualKeyCode key)
	{
		SendMessage(gameWindow, WM_KEYUP, ((IntPtr)key), (IntPtr)0);
	}

	private static void KeyDown(IntPtr gameWindow, VirtualKeyCode key)
	{
		SendMessage(gameWindow, WM_KEYDOWN, ((IntPtr)key), (IntPtr)0);
	}

	private static void KeyPress(IntPtr gameWindow, VirtualKeyCode key)
	{
		KeyDown(gameWindow, key);
		KeyUp(gameWindow, key);
	}
	
	public static void SendKey(IntPtr gameWindow, VirtualKeyCode key, VirtualKeyCode? modifier = null)
	{
		using (var modifierKey = new ModifierKey(gameWindow, modifier))
		{
			KeyPress(gameWindow, key);
		}
	}

	public static void SendKey(IntPtr gameWindow, uint code, uint modif = 0)
	{
		var VK_KEY = (VirtualKeyCode)Enum.ToObject(typeof(VirtualKeyCode), code);
		if (modif>0)
		{
			var VK_MOD = (VirtualKeyCode)Enum.ToObject(typeof(VirtualKeyCode), modif);
			using (var modifierKey = new ModifierKey(gameWindow, VK_MOD))
			{
				KeyPress(gameWindow, VK_KEY);
			}
		}
		else {
			using (var modifierKey = new ModifierKey(gameWindow, null))
			{
				KeyPress(gameWindow, VK_KEY);
			}
		}
	}
}
