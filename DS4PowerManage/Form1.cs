﻿// Sony USB Wireless USB Adapter CUH_ZWA1J
#define USE_SONYUSBADPTER
using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices; // Dllimport
using Microsoft.DirectX.DirectInput;
using static InputSimulator;
namespace App
{
	public partial class Form1 : Form
	{
		private static string CurrentDir = System.AppDomain.CurrentDomain.BaseDirectory;
		private static System.Diagnostics.DefaultTraceListener sDtl;
		private static Form1 sForm1;
		private static Device sJoystick = null;
		private static JoystickState rjoystate;
		private static System.Windows.Forms.Timer timer1 = null;
		private static System.Windows.Forms.Timer timer2 = null;
		private static bool sIsActive = false;
		private static TimeSpan sSuspendInterval = new TimeSpan(0, 5, 0); // 5 minutes
		private static TimeSpan sDefaultInterval = new TimeSpan(3, 0, 0); // 3 hours
		private static DateTime sSuspendTime = DateTime.Now + sDefaultInterval;
		private static DateTime rSuspendTime = DateTime.Now;
		private static byte[] rbuttons = null;
		private const uint PAD_BTN_L3 = 10;
		private const uint PAD_BTN_R3 = 11;
		private const uint PAD_BTN_PS = 12;
		private const uint PAD_BTN_L2 = 6;
		private const uint PAD_BTN_R2 = 7;
		private const uint PAD_BTN_TOUCHPAD = 13;

		public static void dprint(string p, params object[] args)
		{
			var str = String.Format(p, args);
			System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str);
			sForm1.textBox1.AppendText(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str +"\r\n");
		}

		public Form1()
		{
			sForm1 = this;
			// コンソールログをファイルに出力
			string logfile = Environment.GetEnvironmentVariable("TEMP") + @"\DS4_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".txt";
			System.IO.File.Delete(logfile);
			sDtl = (System.Diagnostics.DefaultTraceListener)System.Diagnostics.Trace.Listeners["Default"];
			sDtl.LogFileName = logfile;
			System.Diagnostics.Debug.WriteLine(logfile);
			InitializeComponent();
			// ProsperoRemoteViewer
		}

		private void SystemEvents_SessionEnding(object sender, Microsoft.Win32.SessionEndingEventArgs e)
		{
			#if false
			if (e.Reason == Microsoft.Win32.SessionEndReasons.SystemShutdown)
			{
				Microsoft.Win32.RegistryKey regs = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", true);
				if (regs != null)
				{
					regs.SetValue("DisableCAD", 1);
					regs.Close();
				}
			}
			#endif
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Properties.Settings.Default.Reload();
			{
				sForm1.checkBox1.Checked = Properties.Settings.Default.Checkbox1;
			}
			this.WindowState = FormWindowState.Minimized;

			//Microsoft.Win32.SystemEvents.SessionEnding += new  Microsoft.Win32.SessionEndingEventHandler(SystemEvents_SessionEnding);

			foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
			{
				if (p.MainWindowHandle != IntPtr.Zero)
					dprint("[{0}] ({1})", p.MainWindowHandle.ToString("x6"), p.ProcessName);
			}

			timer1 = new System.Windows.Forms.Timer();
			timer1.Enabled = true;
			timer1.Interval = 1000; // 1 sec when no controller
			timer1.Tick += delegate
			{
				if (sJoystick == null)
				{
					timer1.Interval = 1000; // 1 sec when no controller
					sJoystick = GetCurrentController();
					if (sJoystick != null)
					{
						timer1.Interval = 3000; // 1 sec when no controller
						sSuspendTime = DateTime.Now + sSuspendInterval;
						#if !(USE_SONYUSBADPTER)
						dprint("Connected [{0}]", sJoystick.DeviceInformation.ProductName);
						var sound = new System.Media.SoundPlayer(Properties.Resources.Windows_Hardware_Insert);
						sound.Play();
						#endif
					}
					return;
				}
				#if (USE_SONYUSBADPTER)
				if (timer1.Interval > 1000)
				{
					dprint("{0}", timer1.Interval);
					rjoystate = sJoystick.CurrentJoystickState;
					dprint("LX/LY {0}/{1}", rjoystate.X, rjoystate.Y); // Left Stick
					dprint("RX/RY {0}/{1}", rjoystate.Z, rjoystate.Rz); // Right Stick
					dprint("L2/R2 {0} {1}", rjoystate.Rx, rjoystate.Ry); // L2 / R2
					rSuspendTime = sSuspendTime;
				}
				#endif
				if (rSuspendTime != sSuspendTime)
				{
					if (DateTime.Now > sSuspendTime)
					{
						rSuspendTime = sSuspendTime;
						BtDisconnect();
						return;
					} else {
						TimeSpan ts = DateTime.Now - sSuspendTime;
						sForm1.label1.Text = ts.ToString(@"hh\:mm\:ss");
					}
				}

				timer1.Interval = 32; // 32
				JoystickState state;
				try { state = sJoystick.CurrentJoystickState; }
				catch (Microsoft.DirectX.DirectInput.InputLostException)
				{
					timer1.Interval = 3000; // 1 sec when no controller
					var sound = new System.Media.SoundPlayer(Properties.Resources.Windows_Hardware_Remove);
					sound.Play();
					dprint("Disconnected");
					sJoystick = null;
					rbuttons = null;
					return;
				}
				sIsActive = false;
				var buttons = state.GetButtons(); // Button
				if (rbuttons == null) {rbuttons = new byte[buttons.Length]; Array.Clear(rbuttons, 0, rbuttons.Length);}
				#if (USE_SONYUSBADPTER)
				if (rSuspendTime == sSuspendTime)
				{
					if (state.X != rjoystate.X || state.Y != rjoystate.Y)
					{
						dprint("LX/LY {0}/{1}", state.X, state.Y); // Left Stick
						sIsActive = true;
					}
					if (state.Z != rjoystate.Z || state.Rz != rjoystate.Rz)
					{
						dprint("RX/RY {0}/{1}", state.Z, state.Rz); // Right Stick
						sIsActive = true;
					}
					if (state.Rx != rjoystate.Rx || state.Ry != rjoystate.Ry)
					{
						dprint("L2/R2 {0} {1}", state.Rx, state.Ry); // L2 / R2
						sIsActive = true;
					}
					if (sIsActive)
					{
						dprint("Connected [{0}]", sJoystick.DeviceInformation.ProductName);
						var sound = new System.Media.SoundPlayer(Properties.Resources.Windows_Hardware_Insert);
						sound.Play();
					}
				}
				rjoystate = state;
				#endif
				//dprint("LX/LY {0}/{1}", state.X, state.Y); // Left Stick
				//dprint("RX/RY {0}/{1}", state.Z, state.Rz); // Right Stick
				//dprint("L2/R2 {0} {1}", state.Rx, state.Ry); // L2 / R2
				for (int ndx=0; ndx<buttons.Length; ndx++)
				{
					if (ndx == PAD_BTN_PS) continue;
					if (buttons[ndx] == 0) continue;
					if (rbuttons[ndx] == 0) dprint("{0}", ndx);
					sIsActive = true;
					break;
				}
				Int32[] pov = state.GetPointOfView(); // Hat Switch
				if (pov[0]>=0) sIsActive = true;
				if (sIsActive)
				{
					sSuspendTime = DateTime.Now + sSuspendInterval;
				}
				buttons.CopyTo(rbuttons, 0);
			};
			timer2 = new System.Windows.Forms.Timer();
			timer2.Enabled = true;
			timer2.Interval = 1000; // 1 sec first time
			timer2.Tick += delegate
			{
				timer2.Interval = 600 * 1000; // 10 minutes
				Microsoft.Win32.RegistryKey regs = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", true);
				if (regs != null)
				{
					if ((int)regs.GetValue("DisableCAD")==0)
					{
						dprint("DisableCAD:{0}",regs.GetValue("DisableCAD"));
						//regs.SetValue("DisableCAD", 1, Microsoft.Win32.RegistryValueKind.DWord);
						regs.SetValue("DisableCAD", 1);
					}
					regs.Close();
				}
				else
					dprint("Registory entry does not exist.");
			};
		}

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			dprint("Form1_FormClosed");
			sDtl.Close();
			Properties.Settings.Default.Checkbox1 = sForm1.checkBox1.Checked;
			Properties.Settings.Default.Save();
		}

		private Device GetCurrentController()
		{
			// ゲームコントローラのデバイス一覧を取得
			DeviceList devList = Manager.GetDevices( DeviceClass.GameControl, EnumDevicesFlags.AttachedOnly );
			// 全てのコントローラを列挙
			foreach( DeviceInstance dev in devList )
			{
				var joy = new Device( dev.InstanceGuid );
				//dprint("Controller [{0}]", joy.DeviceInformation.ProductName);
				joy.SetCooperativeLevel( this, CooperativeLevelFlags.Background | CooperativeLevelFlags.NonExclusive );
				joy.SetDataFormat(DeviceDataFormat.Joystick);
				joy.Acquire();
				return joy;
			}
			return null;
		}

		private bool DevCon(string cmds, out string stdout)
		{
			var p = new System.Diagnostics.Process();
			p.StartInfo.FileName = CurrentDir + @"Resources\devcon.exe";
			p.StartInfo.Arguments = cmds;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardOutput = true;
			p.Start();
			stdout = p.StandardOutput.ReadToEnd();
			p.WaitForExit();
			var result = p.ExitCode;
			p.Dispose();
			return result == 0 ? true:false;
		}

		private void BtDisconnect()
		{
			string stdout;
			DevCon("hwids *", out stdout);
			List<string> devices = new List<string>();
			foreach(var line in stdout.Split(new[] { Environment.NewLine },StringSplitOptions.None))
			{
				if (System.Text.RegularExpressions.Regex.IsMatch(line, @"HID\\{.*_VID&0002054c_PID&09cc"))
					devices.Add(line.Trim().Replace("HID", "BTHENUM"));
				if (line.EndsWith(@"USB\VID_054C&PID_0BA0&REV_0100"))
					devices.Add(line.Trim());
			}
			if (devices.Count==0)
			{
				dprint("no controller");
				return;
			}
			foreach(var device in devices)
			{
				DevCon("disable " + device, out stdout);
				dprint("{0}", stdout);
			}
			System.Threading.Thread.Sleep(1 * 2000);
			foreach(var device in devices)
			{
				DevCon("enable " + device, out stdout);
				dprint("{0}", stdout);
			}
		}

		private void DisableCAD()
		{
			Microsoft.Win32.RegistryKey regs = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", true);
			if (regs != null)
			{
				regs.SetValue("DisableCAD", 1);
				regs.Close();
			}
		}

		//////////////////////////////////////////////////////////////////////
		//
		// Form API
		//
		private void Form1_Shown(object sender, EventArgs e)
		{
			//this.Hide();
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
		}

		private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
		{
			//dprint("notifyIcon1_MouseClick");			// フォームを表示する
			this.Visible = true;
			// 現在の状態が最小化の状態であれば通常の状態に戻す
			if (this.WindowState == FormWindowState.Minimized)
			{
				this.WindowState = FormWindowState.Normal;
			}
			this.Activate(); // フォームをアクティブにする
			this.textBox1.ScrollToCaret(); // フォーカスを行末に移動
		}

		private void Form1_MinimumSizeChanged(object sender, EventArgs e)
		{
			//dprint("Form1_MinimumSizeChanged");
			// フォームが最小化の状態であればフォームを非表示にする
			this.Hide();
		}

		private void Form1_ClientSizeChanged(object sender, EventArgs e)
		{
			if (this.WindowState == System.Windows.Forms.FormWindowState.Minimized)
			{
				//dprint("Form1_ClientSizeChanged Minimized");
				// フォームが最小化の状態であればフォームを非表示にする
				this.Hide();
			}
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{

		}
	}
}
