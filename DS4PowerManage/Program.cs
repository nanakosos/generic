﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
	static class Program
	{
		/// <summary>
		/// アプリケーションのメイン エントリ ポイントです。
		/// </summary>
		[STAThread]
		static void Main()
		{
			//ミューテックス作成
			var _mutex = new System.Threading.Mutex(false, "DS4PWM");
			//ミューテックスの所有権を要求する
			if (_mutex.WaitOne(0, false) == false) {
				MessageBox.Show("本ソフトウェアは複数起動できません。", "FFXIV");
				return;
			}
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form1());
		}
	}
}
