﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Waterfox
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			#if false
			// コンソールログをファイルに出力
			string logfile = Environment.GetEnvironmentVariable("TEMP") + @"\MACTYPE_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".txt";
			System.Diagnostics.Debug.WriteLine(logfile);
			System.IO.File.Delete(logfile);
			System.Diagnostics.DefaultTraceListener dtl = (System.Diagnostics.DefaultTraceListener)System.Diagnostics.Debug.Listeners["Default"];
			dtl.LogFileName = logfile;
			#endif
			InitializeComponent();
		}
		private void Form1_Shown(object sender, EventArgs e)
		{
			Visible = false;
		}
		private void Form1_Load(object sender, EventArgs e)
		{
			string MacLoader = Environment.GetEnvironmentVariable("X_MACLOADER_EXE");
			if (String.IsNullOrEmpty(MacLoader))
			{
				MacLoader = @"D:\INSTALL\MacType\MacTypePortable\MacLoader64.exe";
				if (System.IO.File.Exists(MacLoader) == false) MacLoader = @"C:\Users\0300800012\Home\INSTALL\MacType\MacTypePortable\MacLoader64.exe";
			}
			string Browser = Environment.GetEnvironmentVariable("X_WATERFOX_EXE");
			if (String.IsNullOrEmpty(Browser))
			{
				Browser = @"C:\Program Files\Waterfox Classic\waterfox.exe";
				if (System.IO.File.Exists(Browser) == false) Browser = @"D:\INSTALL\Firefox\WaterfoxPortable\App\Waterfox\waterfox.exe";
				if (System.IO.File.Exists(Browser) == false) Browser = @"C:\Users\0300800012\Home\INSTALL\Firefox\WaterfoxPortable\App\Waterfox\waterfox.exe";
			}
			string param = System.Environment.CommandLine;
			dprint(param);
			param = param.Replace(System.Environment.GetCommandLineArgs()[0], "").Replace(@"""", "");
			param = "\"" + Browser + "\" " + param;
			ProcessExecute(MacLoader, param);
			this.Close();
		}
		public static void dprint(string p, params object[] args)
		{
			var str = String.Format(p, args);
			System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str);
			//sForm1.textBox1.AppendText(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str +"\r\n");
		}
		private bool ProcessExecute(string exe, string param = "")
		{
			dprint(exe);
			dprint(param);
			var result = 0;
			dprint(exe + " " + param);
			var p = new System.Diagnostics.Process();
			p.StartInfo.FileName = exe;
			p.StartInfo.Arguments = param;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.Start();
			p.WaitForExit();
			result = p.ExitCode;
			p.Dispose();
			return result == 0 ? true:false;
		}
	}
}
