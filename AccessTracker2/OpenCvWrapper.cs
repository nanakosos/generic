using System;
using System.Collections.Generic;
using System.Drawing;
using OpenCvSharp;

	public static class OpenCvWrapper
	{
		public static bool TemplateMatching(Bitmap src, Bitmap templ, out System.Drawing.Point rout, double precision = 0.88)
		{
			var screen = OpenCvSharp.Extensions.BitmapConverter.ToMat(src);
			var template = OpenCvSharp.Extensions.BitmapConverter.ToMat(templ);

			var result = new OpenCvSharp.Mat();
			OpenCvSharp.Cv2.MatchTemplate(screen, template, result, OpenCvSharp.TemplateMatchModes.CCoeffNormed);
			OpenCvSharp.Cv2.Threshold(result, result, precision, 1.0, OpenCvSharp.ThresholdTypes.Binary);
			OpenCvSharp.Cv2.MinMaxLoc(result, out OpenCvSharp.Point minPoint, out OpenCvSharp.Point maxPoint);

			screen.Dispose();
			template.Dispose();
			result.Dispose();
			//System.Diagnostics.Debug.WriteLine("HUD Party Info {0}", maxPoint);
			//screen.Rectangle(maxPoint, new OpenCvSharp.Point(maxPoint.X + template.Width, maxPoint.Y + template.Height), Scalar.Red);
			rout = new System.Drawing.Point();
			if (maxPoint.X == 0 && maxPoint.Y == 0) return false;
			rout.X = maxPoint.X;
			rout.Y = maxPoint.Y;
			return true;
		}

		public static bool TemplateMatchingHigh(Bitmap src, Bitmap templ, out System.Drawing.Point rout)
		{
			return TemplateMatching(src, templ, out rout, 0.972);
		}
	}
