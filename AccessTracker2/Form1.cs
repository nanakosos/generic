﻿using System;
using System.Runtime.InteropServices; // LayoutKind
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static AccessTracker.Win32;

namespace AccessTracker
{
	public partial class Form1 : Form
	{
		private static string sInFile = @"D:\INSTALL\LiveStream\OBS_Streaming_nginx_v1\logs\access.log";
		private static System.IO.FileStream fs = null;
		private static System.IO.TextReader sr = null;
		private static Form1 sForm1;
		private static System.Windows.Forms.Timer sTimer = null;
		private static System.Windows.Forms.Timer sTimer1 = null;
		public Form1()
		{
			sForm1 = this;
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			//ScreenShotFromExMonitor();
			string[] args = System.Environment.GetCommandLineArgs();
			if (args.Length>1) sInFile = args[0];
			sTimer = new System.Windows.Forms.Timer();
			sTimer.Enabled = true;
			sTimer.Interval = 2000; // 2 sec when no controller
			sTimer.Tick += delegate
			{
				DAZN_Shine();
			};
			if (System.IO.File.Exists(sInFile))
			{
				dprint(sInFile);
				var sViewers = new Dictionary<string, int>();
				fs = new System.IO.FileStream( sInFile, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
				sr = new System.IO.StreamReader(fs);
				sr.ReadToEnd();
				sViewers.Clear();
				sTimer1 = new System.Windows.Forms.Timer();
				sTimer1.Enabled = true;
				sTimer1.Interval = 1000; // 1 sec when no controller
				sTimer1.Tick += delegate
				{
					sTimer1.Interval = 1000; // 2 sec when no controller
					string line = sr.ReadLine();
					if (!System.String.IsNullOrEmpty(line))
					{
						//dprint(line);
						sTimer1.Interval = 200;
						if(line.Contains(" - - ") && line.Contains("GET "))
						{
							var visitor = line.Substring(0, line.IndexOf(" - - "));
							if (sViewers.ContainsKey(visitor)==false)
							{
								try {
									System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(visitor);
									dprint("{0} [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, host.HostName);
								}
								catch {
									dprint("{0} [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, "---");
								}
								System.Media.SystemSounds.Question.Play();
								//System.Media.SystemSounds.Hand.Play();
								//System.Media.SystemSounds.Asterisk.Play();
								//System.Media.SystemSounds.Exclamation.Play();
								//System.Media.SystemSounds.Question.Play();
							}
							sViewers[visitor] = 10;
						}
					}
					if (sViewers.Count>0)
					{
						List<string> keysList = new List<string>(sViewers.Keys);
						foreach (var visitor in keysList)
						{
							if (sViewers[visitor]==0)
							{
								try {
									System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(visitor);
									dprint("{0} Exit [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, host.HostName);
								}
								catch { // System.Net.Sockets.SocketException
									dprint("{0} Exit [ {1} ] ({2})", DateTime.Now.ToString("HH:mm:ss"), visitor, "---");
								}
								//System.Media.SystemSounds.Hand.Play();
								sViewers.Remove(visitor);
							}
							else sViewers[visitor] = sViewers[visitor] - 1;
						}
					}
				};
			}
			else
			{
				dprint("{0} does not exist.", sInFile);
			}
        }

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			sForm1.ProcessExecute(@"D:\INSTALL\LiveStream\OBS_Streaming_nginx_v1\nginx.exe", "-s stop");
		}
		//////////////////////////////////////////////////////////////////////
		//
		//
		public static void print(object s)
		{
			print("{0}", s);
		}
		public static void print(string p, params object[] args)
		{
			var str = String.Format(p, args);
			System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str);
			sForm1.textBox1.AppendText(DateTime.Now.ToString("HH:mm:ss.fff") + " " + str +"\r\n");
		}
		public static void dprint(string p, params object[] args)
		{
			//if (sForm1.checkBox4.Checked == false) return;
			print(p,args);
		}
		public static void dprint(object s)
		{
			//if (sForm1.checkBox4.Checked == false) return;
			print("{0}", s);
		}
		//////////////////////////////////////////////////////////////////////
		//
		//
		private bool ProcessExecute(string exe, string param)
		{
			dprint(exe + " " + param);
			var p = new System.Diagnostics.Process();
			p.StartInfo.FileName = exe;
			p.StartInfo.Arguments = param;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.Start();
			p.WaitForExit();
			var result = p.ExitCode;
			p.Dispose();
			return result == 0 ? true:false;
		}

		private static void ScreenShotFromExMonitor()
		{
			Bitmap img = ScreenCaptureFromExMonitor();
			if (img == null) return;
			img.Save(DateTime.Now.ToString("yyyyMMddTHHmmss")+".PNG");
			img.Dispose();
		}
		private static System.Drawing.Bitmap ScreenCaptureFromExMonitor()
		{
			Rectangle rectangle = new Rectangle(0, -1080, 1920, 1080);
			return ScreenCaptureFromExMonitor(rectangle);
		}
		private static System.Drawing.Bitmap ScreenCaptureFromExMonitor(Rectangle rectangle)
		{
			//Rectangle rectangle = new Rectangle(0, -1080, 1920, 1080);
			Bitmap bitmap = new Bitmap(rectangle.Width, rectangle.Height);
			Graphics graphics = Graphics.FromImage(bitmap);
			// 画面をコピー
			try {graphics.CopyFromScreen(new Point(rectangle.X, rectangle.Y), new Point(0, 0), rectangle.Size);}
			catch {}
			graphics.Dispose();
			//bitmap.Save(DateTime.Now.ToString("yyyyMMddTHHmmss")+".PNG");
			return bitmap;
		}
		private static System.Drawing.Bitmap ScreenCapture(IntPtr hWnd)
		{
			RECT32 rout;
			if (IsIconic(hWnd) == true) return null;
			if (GetClientRect(hWnd, out rout) == false) return null;
			Rectangle rectangle = new Rectangle(0, -1080, 1920, 1080);
			//Bitmap bmp = new Bitmap(rectangle.Width, rout.bottom - rout.top + SystemInformation.CaptionHeight + 3);
			Bitmap bmp = new Bitmap(1920, 1080);
			Graphics memg = Graphics.FromImage(bmp);
			IntPtr dc = memg.GetHdc();
			PrintWindow(hWnd, dc, 0);
			memg.ReleaseHdc(dc);
			memg.Dispose();
			//bmp.Save(DateTime.Now.ToString("yyyyMMddTHHmmss")+".BMP.PNG");
			dprint("{0}x{1}", bmp.Width, bmp.Height);
			//System.Drawing.Rectangle rect = new System.Drawing.Rectangle(rout.left + 3, rout.top+SystemInformation.CaptionHeight + 3, rout.right - rout.left, rout.bottom - rout.top);
			//var img = bmp.Clone(rect, bmp.PixelFormat);
			//bmp.Dispose();
			//dprint("{0}x{1}", img.Width, img.Height);
			return bmp;
		}
		private IntPtr GetChromeWindow()
		{
			foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("chrome"))
			{
				IntPtr hWnd = p.MainWindowHandle;
				if (hWnd != IntPtr.Zero) return hWnd;
			}
			return IntPtr.Zero;
		}
		/*********************************************************************
		  Mouse Event
		*********************************************************************/
		private const int MOUSEEVENTF_LEFTDOWN = 0x02;
		private const int MOUSEEVENTF_LEFTUP = 0x04;
		[StructLayout(LayoutKind.Sequential, Pack = 4)]
		struct POINT32
		{
			public int X { get; set; }
			public int Y { get; set; }
			public static implicit operator System.Drawing.Point(POINT32 point) { return new System.Drawing.Point(point.X, point.Y); }
		}
		[DllImport("User32.dll")]
		private static extern bool SetCursorPos(int X, int Y);
		[DllImport("User32.dll")]
		private static extern bool GetCursorPos(out POINT32 lppoint);
		[DllImport("USER32.dll", CallingConvention = CallingConvention.StdCall)]
		private static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);
		private void DAZN_Shine()
		{
			Point rout;
			IntPtr hWnd = GetChromeWindow();
			//IntPtr hWnd = FindWindow("Chrome_WidgetWin_1", null);
			if (hWnd == IntPtr.Zero) return;
			if (sForm1.checkBox1.Checked == false) return;
			//dprint("Capture {0}x{1}", img.Width, img.Height);
			if (sForm1.checkBox1.Checked)
			{
				Rectangle rectangle = new Rectangle(1920/2 - 240, -1080/2 - 80, 480, 60);
				Bitmap img = ScreenCaptureFromExMonitor(rectangle);
				//Bitmap img = ScreenCaptureFromExMonitor();
				if (OpenCvWrapper.TemplateMatching(img, Properties.Resources.DAZN_SHINE, out rout)==true)
				{
					dprint("{0} DAZN SHINE!!!! {1}", DateTime.Now.ToString("HH:mm:ss"), rout);
					POINT32 rPoint;
					GetCursorPos(out rPoint);
					dprint("Mouse Click {0},{1}", rout.X + rectangle.Left + 40, rout.Y + rectangle.Top + 20);
					SetCursorPos(rout.X + rectangle.Left + 40, rout.Y + rectangle.Top + 20);
					mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
					mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
					SetCursorPos(rPoint.X, rPoint.Y);
					img.Dispose();
					return;
				}
				img.Dispose();
			}
			if (sForm1.checkBox2.Checked)
			{
				Rectangle rectangle = new Rectangle(64, -1080+879, 580, 160);
				Bitmap img = ScreenCaptureFromExMonitor(rectangle);
				if (OpenCvWrapper.TemplateMatching(img, Properties.Resources.LIVE, out rout)==true)
				{
					dprint("{0} Live Detect {1}", DateTime.Now.ToString("HH:mm:ss"), rout);
					Point routzone;
					System.Drawing.Rectangle rect = new System.Drawing.Rectangle(rout.X, rout.Y, 300, 120);
					var imgLive = img.Clone(rect, img.PixelFormat);
					if (OpenCvWrapper.TemplateMatching(imgLive, Properties.Resources.ZONE, out routzone)==true)
					{
						if (sForm1.checkBox3.Checked == false) rout.X += 300;
					}
					else {
						if (sForm1.checkBox3.Checked == true) rout.X += 300;
					}
					imgLive.Dispose();
					POINT32 rPoint;
					GetCursorPos(out rPoint);
					//SetCursorPos(1920/2 + 60, (-1080/2)-40);
					dprint("Mouse Click {0},{1}", rout.X  + rectangle.Left + 80, rout.Y + rectangle.Top + 40);
					SetCursorPos(rout.X  + rectangle.Left + 80, rout.Y + rectangle.Top + 40);
					mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
					mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
					SetCursorPos(rPoint.X, rPoint.Y);
					img.Dispose();
					return;
				}
				img.Dispose();
			}
			if (sForm1.checkBox2.Checked)
			{
				Rectangle rectangle = new Rectangle(1240, -1080+860, 260, 20);
				Bitmap img = ScreenCaptureFromExMonitor(rectangle);
				if (OpenCvWrapper.TemplateMatching(img, Properties.Resources.FOLLOW, out rout)==true)
				{
					dprint("{0} Change to Full Screen", DateTime.Now.ToString("HH:mm:ss"));
					POINT32 rPoint;
					GetCursorPos(out rPoint);
					//SetCursorPos(1920/2 + 60, (-1080/2)-40);
					SetCursorPos(rout.X  + rectangle.Left, -1080/2);
					mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
					mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
					System.Threading.Thread.Sleep(250);
					mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
					mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
					SetCursorPos(rPoint.X, rPoint.Y);
					img.Dispose();
					return;
				}
				img.Dispose();
			}
		}

		private void button1_MouseClick(object sender, MouseEventArgs e)
		{
			RECT32 rect;
			IntPtr hWnd = GetChromeWindow();
			if (hWnd == IntPtr.Zero) return;
			GetWindowRect(hWnd, out rect);
			//dprint("({0},{1}) - ({2},{3})", rect.left, rect.top, rect.right, rect.bottom);
			//dprint("({0},{1}) - ({2},{3})", rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top );
			Button btn=(Button)sender;
			if (btn.Text == "FullScrn")
			{
				if (rect.top < -16)
				{
					SetForegroundWindow(hWnd);
					System.Threading.Thread.Sleep(200);
					System.Windows.Forms.SendKeys.Send("+f");
				}
				else
				{
					SetWindowPos(hWnd, HWND_TOP, 18, 0, 1280, 1024, 0);
				}
			}
			else if (btn.Text == "MoveScrn")
			{
				if (rect.top < -16)
				{
					SetForegroundWindow(hWnd);
					System.Threading.Thread.Sleep(200);
					System.Windows.Forms.SendKeys.Send("{ESC}");
					System.Threading.Thread.Sleep(200);
					SetWindowPos(hWnd, HWND_TOP, 18, 0, 1280, 1024, 0);
				}
				else
				{
					ShowWindow(hWnd, SW_RESTORE);
					System.Threading.Thread.Sleep(200);
					SetWindowPos(hWnd, HWND_TOP, -8, -1088, 0, 0, SWP_NOSIZE);
					SetForegroundWindow(hWnd);
					//System.Windows.Forms.SendKeys.Send("+f");
					System.Threading.Thread.Sleep(200);
				}
			}
			else if (btn.Text == "<")
			{
				SetForegroundWindow(hWnd);
				System.Windows.Forms.SendKeys.Send("{LEFT}");
				System.Threading.Thread.Sleep(200);
			}
			else if (btn.Text == ">")
			{
				SetForegroundWindow(hWnd);
				System.Windows.Forms.SendKeys.Send("{RIGHT}");
				System.Threading.Thread.Sleep(200);
			}
			else if (btn.Text == "||")
			{
				SetForegroundWindow(hWnd);
				System.Windows.Forms.SendKeys.Send(" ");
				System.Threading.Thread.Sleep(200);
			}
		}
	}

	public class Win32
	{
		[DllImport("user32.dll")]
		public static extern uint GetWindowLong(IntPtr hWnd, int nIndex);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern int GetClassName(IntPtr hWnd, StringBuilder s, int nMaxCount);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern bool IsWindow(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr GetForegroundWindow();
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr FindWindow(String sClassName, String sWindowText);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr FindWindowEx(IntPtr hWnd, IntPtr hwndChildAfter, String lpszClass, String lpszWindow);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern bool SetForegroundWindow(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern bool PostMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		public static extern Int32 SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32.dll")]
		public static extern bool IsIconic(IntPtr hWnd);
		[StructLayout(LayoutKind.Sequential, Pack = 4)]
		public struct RECT32 { public int left; public int top; public int right; public int bottom; }
		[System.Runtime.InteropServices.DllImport("User32.dll")]
		public extern static bool PrintWindow(IntPtr hwnd, IntPtr hDC, uint nFlags);
		[System.Runtime.InteropServices.DllImport("User32.dll")]
		public static extern bool GetClientRect(IntPtr hWnd, out RECT32 lpRect);
		[DllImport("user32.dll", SetLastError = true)]
		public static extern int GetWindowThreadProcessId(IntPtr hWnd,out int lpdwProcessId);
		[DllImport("user32.dll")]
		public static extern bool GetWindowRect(IntPtr hWnd, out RECT32 lpRect);
		///////////////////////////////
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int uFlags);
		public const int HWND_BOTTOM = 1;
		public const int HWND_TOP = 0;
		public const int HWND_TOPMOST = -1;
		public const int HWND_NOTOPMOST = -2;
		public const int SWP_NOSIZE = 0x0001;
		public const int SWP_NOMOVE = 0x0002;
		///////////////////////////////
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int ShowWindow(IntPtr hWnd, int nCmdShow);
		public const int SW_MAXIMIZE = 3;
		public const int SW_MINIMIZE = 6;
		public const int SW_RESTORE  = 9;
	}
}
